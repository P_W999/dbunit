/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.service;

import be.pw.dbunittryout.model.AdresEntity;
import be.pw.dbunittryout.model.LandEntity;
import be.pw.dbunittryout.model.PersoonEntity;
import be.pw.dbunittryout.model.Persoon_X_AdresEntity;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.Query;
import org.dbunit.dataset.IDataSet;
import org.junit.Test;

/**
 *
 * @author pw
 */
public class SomeDBUnitTest extends AbstractDBUnitTest {
    
   
    @Test
    public void testLandEntity() throws Exception {
        LandEntity land = em.find(LandEntity.class, 0L);
        assertNotNull(land);
        assertEquals("belgie", land.getLand());
    }
    
    @Test
    public void testPersoonEntity() throws Exception {
        PersoonEntity persoon = em.find(PersoonEntity.class, 0L);
        assertNotNull(persoon);
        assertEquals("Jansens", persoon.getNaam());
    }
    
    @Test
    public void testAdresEntity() throws Exception {
        AdresEntity adres = em.find(AdresEntity.class, 0L);
        assertNotNull(adres);
        assertEquals("Some street 10", adres.getLijn1());
        assertNotNull(adres.getLand());
        assertTrue(adres.getLand().getId().equals(0L));
    }
    
    @Test
    public void testPersoon_X_AdresEntity() throws Exception {
        Persoon_X_AdresEntity pxa = em.find(Persoon_X_AdresEntity.class, 0L);
        assertNotNull(pxa);
        assertNotNull(pxa.getAdres());
        assertTrue(pxa.getAdres().getId().equals(0L));
        assertNotNull(pxa.getPersoon());
        assertTrue(pxa.getPersoon().getId().equals(0L)); 
    }

    @Test
    public void testInsert() {
        //em.getTransaction().begin();
        Long id = null;
        PersoonEntity persoon = new PersoonEntity();
        persoon.setNaam("Michael");
        persoon.setVoornaam("Bonanza");
        em.persist(persoon);
        em.flush();
        //em.getTransaction().commit();
        
        
        id = persoon.getId();
        persoon = null;
        em.clear();
        persoon = em.find(PersoonEntity.class, id);
        assertNotNull(persoon);
        assertEquals("Michael", persoon.getNaam());
    }
    
    @Test
    public void testDBUnitWorkingCorrectly() {
        Query q = em.createNativeQuery("select count(*) from dbunit.persoon;");
        Object o = q.getSingleResult();
        assertNotNull(o);
        BigInteger i = (BigInteger) o;
        assertTrue(i.intValue() == 5);
        
        q = em.createNativeQuery("select * from dbunit.persoon where T_NAAM like '%michael%'");
        Collection c = q.getResultList();
        assertNotNull(c);
        assertEquals(0, c.size());
    }
    

}
