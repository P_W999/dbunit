/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.service.export;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatDtdDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

/**
 *
 * @author pw
 */
public class Export {
    public static void main(String[] args) throws Exception
    {
        // database connection
        Class driverClass = Class.forName("org.h2.Driver");
        Connection jdbcConnection = DriverManager.getConnection("jdbc:h2:tcp://localhost:9101/dbunit", "sa", "");
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
        
        jdbcConnection.createStatement().execute("set schema dbunit");
        
        // full database export
        IDataSet fullDataSet = connection.createDataSet();
        FlatXmlDataSet.write(fullDataSet, new FileOutputStream("full.xml"));
        FlatDtdDataSet.write(connection.createDataSet(), new FileOutputStream("test.dtd"));
        
        // dependent tables database export: export table X and all tables that
        // have a PK which is a FK on X, in the right order for insertion
        //String[] depTableNames = TablesDependencyHelper.getAllDependentTables( connection, "X" );
        //IDataSet depDataset = connection.createDataSet( depTableNames );
        //FlatXmlDataSet.write(depDataset, new FileOutputStream("dependents.xml"));          
        
    }
}
