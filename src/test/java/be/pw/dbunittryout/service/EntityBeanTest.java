/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.service;

import org.junit.rules.Timeout;
import org.junit.rules.MethodRule;
import org.junit.Rule;
import be.pw.dbunittryout.model.AdresEntity;
import be.pw.dbunittryout.model.PersoonEntity;
import be.pw.dbunittryout.model.LandEntity;
import be.pw.dbunittryout.model.Persoon_X_AdresEntity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Doesn't use DBUNIT !!!
 * @author pw
 */
public class EntityBeanTest {
    
    @Rule
    public MethodRule globalTimeout= new Timeout(250);
    private static EntityManager em = null;
    
    @BeforeClass
    public static void setUp() throws Exception {
        em = createEntityManager();
    }
    
    private static EntityManager createEntityManager() throws FileNotFoundException, IOException {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("be.pw_dbunitTryOut_junit");
        EntityManager em = emf.createEntityManager();
        return em;
    }
    
    @Test
    public void testLandEntity() throws Exception {
        LandEntity land = em.find(LandEntity.class, 0L);
        assertNotNull(land);
        assertEquals("belgie", land.getLand());
    }
    
    @Test
    public void testPersoonEntity() throws Exception {
        PersoonEntity persoon = em.find(PersoonEntity.class, 0L);
        assertNotNull(persoon);
        assertEquals("Jansens", persoon.getNaam());
    }
    
    @Test
    public void testAdresEntity() throws Exception {
        AdresEntity adres = em.find(AdresEntity.class, 0L);
        assertNotNull(adres);
        assertEquals("Some street 10", adres.getLijn1());
        assertNotNull(adres.getLand());
        assertTrue(adres.getLand().getId().equals(0L));
    }
    
    @Test
    public void testPersoon_X_AdresEntity() throws Exception {
        Persoon_X_AdresEntity pxa = em.find(Persoon_X_AdresEntity.class, 0L);
        assertNotNull(pxa);
        assertNotNull(pxa.getAdres());
        assertTrue(pxa.getAdres().getId().equals(0L));
        assertNotNull(pxa.getPersoon());
        assertTrue(pxa.getPersoon().getId().equals(0L));
                
    }
 
}
