/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.MethodRule;
import org.junit.rules.Timeout;

/**
 *
 * @author pw
 */
public abstract class AbstractDBUnitTest extends DBTestCase {

    @Rule
    public MethodRule globalTimeout = new Timeout(250);
    private String url = null;
    private String username = null;
    private String password = null;
    private String driver = null;
    private Connection connection = null;
    private FileInputStream is;
    private IDatabaseConnection conn;
    protected IDataSet dataSet;
    protected EntityManager em;

    @Override
    @Before
    protected void setUp() throws Exception {
        super.setUp();
        try {
            connection.createStatement().executeUpdate("set schema dbunit");
            DatabaseOperation.CLEAN_INSERT.execute(conn, dataSet);
            em.getTransaction().begin();
        } catch (Exception e) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.SEVERE, e.getMessage());
        }

    }
    
    @After
    protected void tearDown() throws Exception {
        super.tearDown();
        em.getTransaction().commit();
        
    }

    public AbstractDBUnitTest() {
        em = Persistence.createEntityManagerFactory("be.pw_dbunitTryOut_junit").createEntityManager();
        readJPAProperties();
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);

            createDatabaseSchema();

            is = new FileInputStream("Z:/dev/src/git/dbunitTryOut/full.xml");
            conn = new DatabaseConnection(connection);
            dataSet = new FlatXmlDataSetBuilder().build(is);


        } catch (Exception e) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.SEVERE, e.getMessage());
        }
    }

    private void createDatabaseSchema() throws IOException {
        Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, "Starting beforeClass");
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        FileReader fr = new FileReader(new File(this.getClass().getResource("/databaseSchema.sql").getFile()));
        BufferedReader br = new BufferedReader(fr);
        StringBuffer sb = new StringBuffer();
        while (br.ready()) {
            sb.append(br.readLine());
        }
        String[] queries = sb.toString().split(";");
        for (String s : queries) {
            Logger.getLogger(getClass().getSimpleName()).log(Level.INFO, s);
            Query q = em.createNativeQuery(s);
            q.executeUpdate();
        }
        em.flush();
        em.getTransaction().commit();
    }

    private void readJPAProperties() {
        Map<String, Object> properties = em.getProperties();
        //url = (String) properties.get("connection.url");
        //username = (String) properties.get("connection.username");
        //password = (String) properties.get("connection.password");
        //driver = (String) properties.get("connection.driver_class");
        url = "jdbc:h2:mem:dbunit";
        username = "sa";
        password = "";
        driver = "org.h2.Driver";
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, driver);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, url);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, username);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, password);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "dbunit");

        Logger.getLogger(this.getClass().getSimpleName()).log(Level.INFO, "username=" + username);
        Logger.getLogger(this.getClass().getSimpleName()).log(Level.INFO, "password=" + password);
        Logger.getLogger(this.getClass().getSimpleName()).log(Level.INFO, "driver=" + driver);
        Logger.getLogger(this.getClass().getSimpleName()).log(Level.INFO, "url=" + url);
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return this.dataSet;
    }
}
