/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.ejb.land;

import be.pw.dbunittryout.exceptions.ValidationException;
import be.pw.dbunittryout.model.LandEntity;
import be.pw.dbunittryout.service.AbstractDBUnitTest;
import java.math.BigInteger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



/**
 *
 * @author pw
 */
public class LandServiceTest extends AbstractDBUnitTest {
    LandService service;
    
    @Before
    public void setUp() throws Exception {
        super.setUp();
        service = new LandService();
        service.setEntityManger(em);
    }
    
    @Test
    public void testFind() {
        LandEntity land = service.find(0L);
        assertNotNull(land);
        assertTrue(land.getId().equals(0L));
        assertEquals("belgie", land.getLand());
    }
    
    @Test
    public void testInsert() throws ValidationException {
        LandEntity land = new LandEntity();
        
        land.setLand("equador");
        LandEntity persistedLand = service.insert(land);
        assertNotNull(land);
        assertNotNull(land.getId());
        assertEquals(land, persistedLand);
    }
    
    @Test
    public void testUpdateOnDetached() throws ValidationException {
        LandEntity land = service.find(0L);
        assertNotNull(land);
        assertEquals("belgie", land.getLand());
        service.getEntityManager().detach(land);
        service.getEntityManager().clear();
        
        land.setLand("België");
        service.update(land);
        
        service.getEntityManager().clear();
        
        land = service.find(0L);
        assertNotNull(land);
        assertEquals("België", land.getLand());
    }
    
    @Test
    public void testUpdateOnAttached() throws ValidationException {
        LandEntity land = service.find(0L);
        assertNotNull(land);
        assertEquals("belgie", land.getLand());
        land.setLand("Belgique");
        service.update(land);
        
        service.getEntityManager().clear();
        
        land = service.find(0L);
        assertNotNull(land);
        assertEquals("Belgique", land.getLand());
    }
    
    @Test
    public void testDeleteWithNoCascade() throws ValidationException {
        LandEntity land = service.find(3L);
        assertNotNull(land);
        assertEquals("dadiland", land.getLand());
        
        service.remove(land);
        service.getEntityManager().clear();
        
        land = service.find(3L);
        assertNull(land);
    }
    
    @Test
    public void testDeleteWithCascade() throws ValidationException {
        verifyNumberCascades(1);
        
        LandEntity land = service.find(2L);
        assertNotNull(land);
        assertEquals("duitsland", land.getLand());
        
        service.remove(land);
        service.getEntityManager().clear();
        
        land = service.find(2L);
        assertNull(land);
        
        verifyNumberCascades(0);
        
    }
    
    private void verifyNumberCascades(int expected) {
        Object r = service.getEntityManager().createNativeQuery("select count(N_ID) from DBUNIT.ADRES WHERE N_LAND_ID = 2").getSingleResult();
        assertNotNull(r);
        BigInteger result = (BigInteger) r;
        assertEquals(expected, result.intValue());
    }
}
