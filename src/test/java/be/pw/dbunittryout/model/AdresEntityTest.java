/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.model;

import be.pw.dbunittryout.exceptions.ValidationException;
import be.pw.dbunittryout.util.ValidationUtil;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author pw
 */
public class AdresEntityTest {

    
    private static final String NOTNULL = ".*may not be null.*";
    private static final String SIZE = ".*size must be.*";
    AdresEntity entity;

    @BeforeMethod
    public void setUp() throws ValidationException {
        entity = new AdresEntity();
        entity.setId(1L);
        entity.setLijn1("Niet null, niet te lang, niet te kort");
        entity.setLijn2("Niet null, niet te lang, niet te kort");
        LandEntity land = new LandEntity();
        land.setId(1L);
        land.setLand("Niet te lang, niet te kort");
        entity.setLand(land);
        ValidationUtil.validate(land);
        ValidationUtil.validate(entity);
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = NOTNULL)
    public void testFailValidationOnNullLijn1() throws ValidationException {
        entity.setLijn1(null);
        ValidationUtil.validate(entity);   
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = NOTNULL)
    public void testFailValidationOnNullLijn2() throws ValidationException {
        entity.setLijn2(null);
        ValidationUtil.validate(entity);
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = NOTNULL)
    public void testFailValidationOnNullLand() throws ValidationException {
        entity.setLand(null);
        ValidationUtil.validate(entity);
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = SIZE)
    public void testFailValidationOnSizeLijn1() throws ValidationException {
        entity.setLijn1(" ");
        ValidationUtil.validate(entity);
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = SIZE)
    public void testFailValidationOnSizeLijn2() throws ValidationException {
        entity.setLijn2(" ");
        ValidationUtil.validate(entity);
    }

    @Test(expectedExceptions = ValidationException.class, expectedExceptionsMessageRegExp = NOTNULL)
    public void testFailValidationOnInvalidLand() throws ValidationException {
        entity.getLand().setLand(null);
        ValidationUtil.validate(entity);
    }

    @AfterClass
    public void cleanUp() {
        entity = null;
    }
}
