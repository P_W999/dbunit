drop schema dbunit if exists;
create user if not exists mike password 'mike';
create schema dbunit authorization sa;
set schema dbunit;

create table PERSOON (
    N_ID    BIGINT NOT NULL AUTO_INCREMENT(0,1),
    T_NAAM  VARCHAR(255) NOT NULL,
    T_VOORNAAM  VARCHAR(255) NOT NULL,
    PRIMARY KEY(N_ID)
);

create table ADRES (
    N_ID    BIGINT NOT NULL AUTO_INCREMENT(0,1),
    T_LIJN1  VARCHAR(255) NOT NULL,
    T_LIJN2  VARCHAR(255) NOT NULL,
    N_LAND_ID    BIGINT NOT NULL,
    PRIMARY KEY(N_ID)
);

create table LAND (
    N_ID    BIGINT NOT NULL AUTO_INCREMENT(0,1),
    T_LAND  VARCHAR(255) NOT NULL,
    PRIMARY KEY(N_ID)
);

create table PERSOON_X_ADRES (
    N_ID            BIGINT NOT NULL AUTO_INCREMENT(0,1),
    N_PERSOON_ID    BIGINT NOT NULL,
    N_ADRES_ID      BIGINT NOT NULL,
    PRIMARY KEY(N_ID)
);

alter table ADRES add constraint FK_ADRES_LAND foreign key (N_LAND_ID) REFERENCES LAND(N_ID) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table PERSOON_X_ADRES add constraint FK_PERSOON_X_ADRES_PERSOON foreign key (N_PERSOON_ID) REFERENCES PERSOON(N_ID) ON DELETE RESTRICT ON UPDATE CASCADE;
alter table PERSOON_X_ADRES add constraint FK_PERSOON_X_ADRES_ADRES foreign key (N_ADRES_ID) REFERENCES ADRES(N_ID) ON DELETE RESTRICT ON UPDATE CASCADE;