insert into land(T_LAND) values ('belgie'), ('nederland'), ('duitsland'), ('dadiland');

insert into adres(T_LIJN1, T_LIJN2, N_LAND_ID) values 
    ('Some street 10', 'Some Community', 0), 
    ('Some other street 20', 'Some Other Community', 0),
    ('Some foreign street 50', 'Some Foreign Community', 1),
    ('Another foreign street 40', 'Another Foreign Community', 1),
    ('Some foreign street 10', 'Some Foreign Community', 1),
    ('Some strange street 10', 'Some strange Community', 2);

insert into persoon(T_NAAM, T_VOORNAAM) values 
    ('Jansens', 'Jan'),
    ('Rowe', 'Mike'),
    ('iomega', 'sharkoon'),
    ('nokia', 'hp'),
    ('lg', 'panasonic');

insert into persoon_x_adres(N_PERSOON_ID, N_ADRES_ID) values
    (0,0),
    (0,1),
    (1,2),
    (2,3),
    (3,3),
    (4,0);