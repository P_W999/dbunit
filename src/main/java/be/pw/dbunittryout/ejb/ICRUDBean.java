/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.ejb;

import be.pw.dbunittryout.exceptions.ValidationException;
import javax.ejb.Local;
import javax.persistence.EntityManager;

/**
 *
 * @author pw
 */
public interface ICRUDBean<T> {
    
    T insert(T entity) throws ValidationException;
    
    T update(T entity) throws ValidationException;
    
    void remove(T entity) throws ValidationException;
    
    T find(Long id);
    
    EntityManager getEntityManager();

}
