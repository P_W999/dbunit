/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.ejb;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author pw
 */
public abstract class SuperBean {
    private EntityManager em;
    
    public final EntityManager getEntityManager() {
        if (em == null) {
            em = Persistence.createEntityManagerFactory("be.pw_dbunitTryOut").createEntityManager();
        }
        return em;
    }
    
    public final void setEntityManger(EntityManager em) {
        this.em = em;
    }
    
}
