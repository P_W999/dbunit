/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.ejb.land;

import be.pw.dbunittryout.ejb.ICRUDBean;
import be.pw.dbunittryout.model.LandEntity;
import javax.ejb.Local;

/**
 *
 * @author pw
 */
@Local
public interface LandServiceLocal extends ICRUDBean<LandEntity> {
    public static final String JNDI_NAME = "EJB_LANDSERVICE#be.pw.dbunittryout.ejb.LandServiceLocal";
}
