/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.ejb.land;

import be.pw.dbunittryout.ejb.SuperBean;
import be.pw.dbunittryout.exceptions.ValidationException;
import be.pw.dbunittryout.util.ValidationUtil;
import be.pw.dbunittryout.model.LandEntity;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 *
 * @author pw
 */
@Stateless(name="EJB_LANDSERVICE", mappedName="EJB_LANDSERVICE")
@TransactionManagement(value= TransactionManagementType.CONTAINER)
@TransactionAttribute(value= TransactionAttributeType.REQUIRED)
public class LandService extends SuperBean implements LandServiceLocal {

    @Override
    public LandEntity insert(LandEntity entity) throws ValidationException {
        assert(entity != null);
        assert(entity.getId() == null);
        ValidationUtil.validate(entity);
        getEntityManager().persist(entity);
        getEntityManager().flush();
        return entity;
    }

    @Override
    public LandEntity update(LandEntity entity) throws ValidationException {
        assert(entity != null);
        assert(entity.getId() != null);
        ValidationUtil.validate(entity);
        getEntityManager().merge(entity);
        getEntityManager().flush();
        return entity;
    }

    @Override
    public void remove(LandEntity entity) throws ValidationException {
        assert(entity != null);
        assert(entity.getId() != null);
        getEntityManager().remove(entity);
        getEntityManager().flush();
    }

    @Override
    public LandEntity find(Long id) {
        assert(id != null);
        return getEntityManager().find(LandEntity.class, id);
    }

    
    
}
