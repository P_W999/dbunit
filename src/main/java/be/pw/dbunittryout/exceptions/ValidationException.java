/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.exceptions;

import java.util.Set;

/**
 *
 * @author pw
 */
public class ValidationException extends Exception {
    private Set violations;
    
    public ValidationException(String message, Set violations) {
        super(message);
        this.violations = violations;
    }

    public Set getViolations() {
        return violations;
    }
       
}
