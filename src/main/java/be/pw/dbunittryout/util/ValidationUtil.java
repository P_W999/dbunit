/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.util;

import be.pw.dbunittryout.exceptions.ValidationException;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author pw
 */
public class ValidationUtil {
    public static void validate(Object o) throws ValidationException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Object>> validate = validator.validate(o);
        if (!validate.isEmpty()) {
            String message = "Validation failed: ";
            StringBuilder build = new StringBuilder("Validation failed: ");
            for (ConstraintViolation<Object> cv : validate) {
                build.append(cv.getRootBeanClass());
                build.append(".");
                build.append(cv.getPropertyPath());
                build.append(" ");
                build.append(cv.getMessage());
                build.append("; ");
            }
            throw new ValidationException(build.toString(), validate);
        }
        
    }
}
