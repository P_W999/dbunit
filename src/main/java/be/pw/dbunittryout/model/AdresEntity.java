/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pw
 */
@Entity
@Table(name="ADRES", schema="DBUNIT")
public class AdresEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="N_ID")
    private Long id;
    
    @Column(name="T_LIJN1")
    @NotNull
    @Size(min=5, max=254)
    private String lijn1;
    
    @Column(name="T_LIJN2")
    @NotNull
    @Size(min=5, max=254)
    private String lijn2;
    
    @JoinColumn(name="N_LAND_ID")
    @ManyToOne(targetEntity=be.pw.dbunittryout.model.LandEntity.class, fetch= FetchType.LAZY, cascade= CascadeType.ALL)
    @Valid
    @NotNull
    private LandEntity land;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LandEntity getLand() {
        return land;
    }

    public void setLand(LandEntity land) {
        this.land = land;
    }

    public String getLijn1() {
        return lijn1;
    }

    public void setLijn1(String lijn1) {
        this.lijn1 = lijn1;
    }

    public String getLijn2() {
        return lijn2;
    }

    public void setLijn2(String lijn2) {
        this.lijn2 = lijn2;
    }

    @Override
    public String toString() {
        return "AdresEntity{" + "id=" + id + ", lijn1=" + lijn1 + ", lijn2=" + lijn2 + ", land=" + land + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdresEntity other = (AdresEntity) obj;
        if ((this.lijn1 == null) ? (other.lijn1 != null) : !this.lijn1.equals(other.lijn1)) {
            return false;
        }
        if ((this.lijn2 == null) ? (other.lijn2 != null) : !this.lijn2.equals(other.lijn2)) {
            return false;
        }
        if (this.land != other.land && (this.land == null || !this.land.equals(other.land))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.lijn1 != null ? this.lijn1.hashCode() : 0);
        hash = 97 * hash + (this.lijn2 != null ? this.lijn2.hashCode() : 0);
        hash = 97 * hash + (this.land != null ? this.land.hashCode() : 0);
        return hash;
    }

    
    
    
    
}
