/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pw
 */
@Entity
@Table(name="PERSOON", schema="DBUNIT")
public class PersoonEntity implements Serializable {
    private static final long serialVersionUID = 1L;
   
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="N_ID")
    private Long id;
    
    @Column(name="T_NAAM")
    @NotNull
    @Size(min=2, max=254)
    private String naam;
    
    @NotNull
    @Size(min=2, max=254)
    @Column(name="T_VOORNAAM")
    private String voornaam;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    @Override
    public String toString() {
        return "PersoonEntity{" + "id=" + id + ", naam=" + naam + ", voornaam=" + voornaam + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersoonEntity other = (PersoonEntity) obj;
        if ((this.naam == null) ? (other.naam != null) : !this.naam.equals(other.naam)) {
            return false;
        }
        if ((this.voornaam == null) ? (other.voornaam != null) : !this.voornaam.equals(other.voornaam)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.naam != null ? this.naam.hashCode() : 0);
        hash = 83 * hash + (this.voornaam != null ? this.voornaam.hashCode() : 0);
        return hash;
    }
    

    
    
}
