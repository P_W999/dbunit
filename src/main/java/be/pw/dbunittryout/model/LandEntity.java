/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author pw
 */
@Entity
@Table(name="LAND", schema="DBUNIT")
public class LandEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="N_ID")
    private Long id;
    
    @Column(name="T_LAND")
    @NotNull
    @Size(min=4, max=254)
    private String land;

    
    @OneToMany(fetch= FetchType.LAZY, cascade= CascadeType.ALL, mappedBy="land", targetEntity=be.pw.dbunittryout.model.AdresEntity.class)
    private Collection<AdresEntity> adressen;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "LandEntity{" + "id=" + id + ", land=" + land + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LandEntity other = (LandEntity) obj;
        if ((this.land == null) ? (other.land != null) : !this.land.equals(other.land)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.land != null ? this.land.hashCode() : 0);
        return hash;
    }
    
    
    
}
