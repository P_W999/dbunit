/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.pw.dbunittryout.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *
 * @author pw
 */
@Entity
@Table(name="PERSOON_X_ADRES", schema="DBUNIT")
public class Persoon_X_AdresEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="N_ID")
    private Long id;
    
    @JoinColumn(name="N_PERSOON_ID")
    @ManyToOne(fetch= FetchType.LAZY, targetEntity=be.pw.dbunittryout.model.PersoonEntity.class, cascade= CascadeType.ALL)
    @NotNull
    @Valid
    private PersoonEntity persoon;
    
    @JoinColumn(name="N_ADRES_ID")
    @ManyToOne(fetch= FetchType.LAZY, targetEntity=be.pw.dbunittryout.model.AdresEntity.class, cascade= CascadeType.ALL)
    @NotNull
    @Valid
    private AdresEntity adres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdresEntity getAdres() {
        return adres;
    }

    public void setAdres(AdresEntity adres) {
        this.adres = adres;
    }

    public PersoonEntity getPersoon() {
        return persoon;
    }

    public void setPersoon(PersoonEntity persoon) {
        this.persoon = persoon;
    }

    @Override
    public String toString() {
        return "Persoon_X_AdresEntity{" + "id=" + id + ", persoon=" + persoon + ", adres=" + adres + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persoon_X_AdresEntity other = (Persoon_X_AdresEntity) obj;
        if (this.persoon != other.persoon && (this.persoon == null || !this.persoon.equals(other.persoon))) {
            return false;
        }
        if (this.adres != other.adres && (this.adres == null || !this.adres.equals(other.adres))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.persoon != null ? this.persoon.hashCode() : 0);
        hash = 23 * hash + (this.adres != null ? this.adres.hashCode() : 0);
        return hash;
    }


    
    
    
    
}
